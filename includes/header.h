/*
** header.h for header in /home/maire_j/rendu/PSU_2017_my_select
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Nov 22 12:03:31 2013 
** Last update Sun Nov 24 21:39:06 2013 
*/

#ifndef LIST_H_
# define LIST_H_

struct		s_list
{
  char		*item;
  int		select;
  int		cur_pos;
  struct s_list	*next;
};

int		create_node(struct s_list **, char *, int, int);
void		my_put_list(struct s_list *);
void		del_node(struct s_list *);
int		get_selected_pos(struct s_list *);
int		get_list_size(struct s_list *);
void		validate_list(struct s_list *);
void		select_item(struct s_list *);
void		delete_item(struct s_list *);
int		set_can();
int		unset_can();
void		clear_scrn(int);

#endif /* LIST_H_ */
