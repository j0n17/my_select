##
## Makefile for Makefile in /home/maire_j/rendu/myls-2013-maire_j
## 
## Made by Maire Jonathan
## Login   <maire_j@epitech.net>
## 
## Started on  Mon Oct 28 10:27:34 2013 Maire Jonathan
## Last update Sun Nov 24 21:00:18 2013 
##

CC	= gcc

RM	= rm -f

NAME	= my_select

SRCS	= srcs/main.c \
	  srcs/list_handle.c \
	  srcs/funcs.c \
	  srcs/funcs2.c

OBJS	= $(SRCS:.c=.o)

CFLAGS	= -Iincludes/ \
	  -Ilibmy/

all: $(NAME)

$(NAME): $(OBJS)
	 @$(CC) $(OBJS) -o $(NAME) -lmy -Llibmy/ -lcurses
	 @$(RM) $(OBJS)
clean:
	@$(RM) $(OBJS)

fclean: clean
	@$(RM) $(NAME)

re: fclean all
