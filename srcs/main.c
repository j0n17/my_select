/*
** main.c for ff in /home/maire_j/.Cfiles
** 
** Made by Maire Jonathan
** Login   <maire_j@epitech.net>
** 
** Started on  Tue Nov  5 11:42:06 2013 Maire Jonathan
** Last update Sun Nov 24 22:39:46 2013 
*/

#include <curses.h>
#include <term.h>
#include <termios.h>
#include <stdlib.h>
#include "header.h"
#include "my.h"

void	del_node(struct s_list *list)
{
  struct s_list *del;
  struct s_list *tmp;

  if (get_selected_pos(list) == 1)
    {
      if (get_list_size(list) == 1)
        {
          unset_can();
          exit(EXIT_SUCCESS);
          my_put_list(list);
        }
      del = tmp;
      tmp = tmp->next->next;
      del = del->next;
      del->cur_pos = 1;
      del->next = tmp;
      *list = *del;
      free(del);
    }
}

void	handle_key(struct s_list *list)
{
  char	buff[5];

  while (read(1, buff, 5) != 0 && buff[0] != '\004')
    {
      clear_scrn(1);
      if (my_strncmp(buff, "\033\133\101", 3) == 0)
	set_new_pos_up(list);
      else if (my_strncmp(buff, "\033\133\102", 3) == 0)
	set_new_pos_down(list);
      if (buff[0] == ' ')
        select_item(list);
      else if (buff[0] == 127 || my_strncmp(buff, "\033\133\063", 3) == 0)
        delete_item(list);
      if (buff[0] == '\n')
	{
	  validate_list(list);
	  unset_can();
	  exit(EXIT_SUCCESS);
	}
      my_put_list(list);
    }
}

int	main(int ac, char **av)
{
  struct s_list	*list;
  int		i;

  list = NULL;
  i = 1;
  while (i < ac)
    {
      if (i == (ac - 1))
	create_node(&list, av[i], 0, 1);
      else
	create_node(&list, av[i], 0, 0);
      i = i + 1;
    }
  clear_scrn(1);
  my_put_list(list);
  get_selected_pos(list);
  handle_key(list);
  unset_can();
  return (0);
}
