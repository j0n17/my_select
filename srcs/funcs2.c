/*
** funcs2.c for funcs2.c in /home/maire_j/rendu/PSU_2017_my_select
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Sun Nov 24 21:00:37 2013 
** Last update Sun Nov 24 23:02:59 2013 
*/

#include <stdlib.h>
#include "header.h"
#include "my.h"

int	printchar(int c)
{
  write(1, &c, 1);
  return (c);
}

void    validate_list(struct s_list *list)
{
  struct s_list *tmp;

  tmp = list;
  while (tmp != NULL)
    {
      if (tmp->select == 1)
	{
	  my_putstr(tmp->item);
	  if (tmp->next != NULL)
	    my_putchar(' ');
	}
      tmp = tmp->next;
    }
}

void	clear_scrn(int info)
{
  char	*xterm;
  char	*clr;
  char	*cm;
  char	*kr;

  if ((xterm = getenv("TERM")) == NULL)
    exit(EXIT_SUCCESS);
  if (tgetent(0, xterm) != 1)
    exit(EXIT_SUCCESS);
  if (info == 1)
    tputs(tgetstr("cl", NULL), 1, printchar);
  tputs(tgoto(tgetstr("cm", NULL), 0, 0), 1, printchar);
  set_can();
  my_putstr("\e[35;1m**********[my_select]**********\e[0m\n");
}
