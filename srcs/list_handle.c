/*
** list_handle.c for list_handle in /home/maire_j/rendu/PSU_2017_my_select
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Nov 22 12:42:46 2013 
** Last update Sun Nov 24 23:06:16 2013 
*/
#include <stdlib.h>
#include "my.h"
#include "header.h"

void	delete_item(struct s_list *list)
{
  struct s_list *tmp;

  tmp = list;
  while (tmp->cur_pos != 1 && tmp->next->cur_pos != 1)
    tmp = tmp->next;
  if (get_selected_pos(list) == 1)
      del_node(list);
  else if (tmp->next->next != NULL && tmp->next)
    {
      tmp->cur_pos = 1;
      tmp->next = tmp->next->next;
    }
  else
    {
      tmp->cur_pos = 1;
      tmp->next = NULL;
    }
}

void	my_put_list(struct s_list *list)
{
  struct s_list	*tmp;

  tmp = list;
  while (tmp != NULL)
    {
      if (tmp->select == 1)
	my_putstr("\e[7;32m");
      if (tmp->cur_pos == 1)
        my_putstr("\033[4m\e[1;32m");
      my_putstr(tmp->item);
      my_putstr("\e[0m");
      if (tmp->next != NULL)
	my_putchar('\n');
      tmp = tmp->next;
    }
}

int	get_selected_pos(struct s_list *list)
{
  struct s_list *tmp;
  int	i;

  tmp = list;
  i = 1;
  while (tmp != NULL && tmp->cur_pos != 1)
    {
      if (tmp->cur_pos == 1)
	return (i);
      tmp = tmp->next;
      i = i + 1;
    }
  return (i);
}

int	get_list_size(struct s_list *list)
{
  struct s_list *tmp;
  int	i;

  tmp = list;
  i = 0;
  while (tmp != NULL)
    {
      tmp = tmp->next;
      i = i + 1;
    }
  return (i);
}

int	create_node(struct s_list **list, char *it, int sel, int cur)
{
  struct s_list	*elem;

  if ((elem = malloc(sizeof(*elem))) == 0)
    return (1);
  elem->item = it;
  elem->select = sel;
  elem->cur_pos = cur;
  elem->next = *list;
  *list = elem;
  return (0);
}
