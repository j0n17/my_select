/*
** funcs.c for funcs.c in /home/maire_j/rendu/PSU_2017_my_select
** 
** Made by 
** Login   <maire_j@epitech.net>
** 
** Started on  Fri Nov 22 12:53:32 2013 
** Last update Sun Nov 24 21:47:57 2013 
*/

#include <termios.h>
#include <stdlib.h>
#include <sys/types.h>
#include "header.h"

void	select_item(struct s_list *list)
{
  struct s_list *tmp;

  tmp = list;
  while (tmp != NULL && tmp->cur_pos != 1)
    tmp = tmp->next;
  if (tmp->select == 0)
    tmp->select = 1;
  else
    tmp->select = 0;
  if (tmp->next != NULL)
    {
      tmp->cur_pos = 0;
      tmp = tmp->next;
      tmp->cur_pos = 1;
    }
}

void	set_new_pos_down(struct s_list *list)
{
  struct s_list *tmp;
  int		i;

  i = -1;
  tmp = list;
  while (++i < get_selected_pos(list) - 1 && tmp != NULL)
    tmp = tmp->next;
  tmp->cur_pos = 0;
  if (tmp->next != NULL)
    {
      tmp = tmp->next;
      tmp->cur_pos = 1;
    }
  else
    {
      tmp = list;
      tmp->cur_pos = 1;
    }
}

void	set_new_pos_up(struct s_list *list)
{
  struct s_list *tmp;
  int		i;

  i = -1;
  tmp = list;
  while (++i < get_selected_pos(list) - 2 && tmp != NULL)
      tmp = tmp->next;
  i = -1;
  if (get_selected_pos(list) == 1)
    {
      tmp = list;
      tmp->cur_pos = 0;
      while (++i < get_list_size(list) - 1 && tmp != NULL)
	tmp = tmp->next;
      tmp->cur_pos = 1;
    }
  else
    {
      tmp->cur_pos = 1;
      tmp = tmp->next;
      tmp->cur_pos = 0;
    }
}

int	set_can()
{
  struct termios t;

  if (tcgetattr(0, &t))
    return (1);
  t.c_cc[VMIN] = 1;
  t.c_cc[VTIME] = 0;
  t.c_lflag &= ~ICANON;
  t.c_lflag &= ~ECHO;
  return (tcsetattr(0, TCSADRAIN, &t));
}

int	unset_can()
{
  struct termios t;

  if (tcgetattr(0, &t))
    return (1);
  t.c_lflag |= ICANON;
  t.c_lflag |= ECHO;
  return (tcsetattr(0, TCSADRAIN, &t));
}
